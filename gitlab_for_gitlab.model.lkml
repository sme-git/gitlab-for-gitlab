connection: "sam_mysql"

# include all the views
include: "*.view"

# include all the dashboards

datagroup: gitlab_for_gitlab_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "2 hour"
}

persist_with: gitlab_for_gitlab_default_datagroup

explore: consoles {}

explore: games {}

explore: tacos {}
