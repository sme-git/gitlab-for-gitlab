view: tacos {
  sql_table_name: lookvr_vg.tacos ;;

  dimension: channel {
    type: string
    sql: ${TABLE}.CHANNEL ;;
  }

  dimension: ghandle {
    type: string
    sql: ${TABLE}.GHANDLE ;;
  }

  dimension: gid {
    type: string
    sql: ${TABLE}.GID ;;
  }

  dimension: given_at {
    type: string
    sql: ${TABLE}.GIVEN_AT ;;
  }

  dimension: message {
    type: string
    sql: ${TABLE}.MESSAGE ;;
  }

  dimension: rhandle {
    type: string
    sql: ${TABLE}.RHANDLE ;;
  }

  dimension: rid {
    type: string
    sql: ${TABLE}.RID ;;
  }

  dimension: tacos {
    type: string
    sql: ${TABLE}.TACOS ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}.TAGS ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
